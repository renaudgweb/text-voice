#!/bin/bash

weather LFMH -m > /home/r/Documents/voice/meteo.txt

sed -i '1,4d' /home/r/Documents/voice/meteo.txt
sed -i 's/Temperature/Température/' /home/r/Documents/voice/meteo.txt
sed -i 's/C/degré/' /home/r/Documents/voice/meteo.txt
sed -i 's/Relative Humidity/Humidité/' /home/r/Documents/voice/meteo.txt
sed -i 's/Wind/Vent/' /home/r/Documents/voice/meteo.txt
sed -i 's/KPH/Noeuds/' /home/r/Documents/voice/meteo.txt
sed -i 's/Sky conditions/Nuages/' /home/r/Documents/voice/meteo.txt
sed -i 's/$/./' /home/r/Documents/voice/meteo.txt
